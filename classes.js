import * as THREE from './lib/three.module.js';

class Poi
{
    constructor(id,mesh)
    {
        this.id = id;
        this.mesh = mesh;
        this.x = mesh.position.x;
        this.y = mesh.position.y;

        this.parent = -1;
        this.visited = false;
        this.neighbors = [];
    }

    reset()
    {
        this.parent = -1;
        this.visited = false;
        this.neighbors = [];
    }
}

class DelaunayGraph
{
    constructor(hlines, far, scene)
    {
        this.triangulation = new Delaunator([0,0]);
        this.poi2Ids = [];
        this.allPoi = [];
        this.poiMeshes = [];
        this.hlines = hlines;
        this.triBuffGeoms = [];
        this.triMeshesValid = [];
        this.triMeshesInvalid = [];
        this.validPaths = [];
        this.invalidPaths = [];

        this.lineGoodMaterial = new THREE.LineBasicMaterial({color: 0x0000ff,linewidth: 2});
        this.lineBadMaterial = new THREE.LineDashedMaterial({color: 0xff5500,linewidth: 2 ,scale: 1,dashSize: 3, gapSize: 3}); 
        this.far = far;
        this.scene = scene;

        this.showGraph = true;
        this.hideInvalid = false;
        this.showPofI = true;
    }

    setStart(mesh)
    {
        //don't add if overlapping
        for(let i = 0; i < this.allPoi.length; ++i)
        {
            if(this.allPoi[i].x == mesh.position.x && this.allPoi[i].y == mesh.position.y)
            {
                this.scene.remove(mesh);
                return 0;
            }
        }
        //replace old one
        if(this.allPoi.length>0)
        {
            this.scene.remove(this.allPoi[0].mesh);
        }
        this.allPoi[0] = new Poi(0,mesh);
    }
    setEnd(mesh)
    {
        //don't add if overlapping
        for(let i = 0; i < this.allPoi.length; ++i)
        {
            if(this.allPoi[i].x == mesh.position.x && this.allPoi[i].y == mesh.position.y)
            {
                this.scene.remove(mesh);
                return 0;
            }
        }
        //replace old one
        if(this.allPoi.length>1)
        {
            this.scene.remove(this.allPoi[1].mesh);
        }
        this.allPoi[1] = new Poi(1,mesh);
    }
    addPoi(mesh, isSecondary)
    {
        for(let i = 0; i < this.allPoi.length; ++i)
        {
            if(this.allPoi[i].x == mesh.position.x && this.allPoi[i].y == mesh.position.y)
            {
                this.scene.remove(mesh);
                return 0;//don't add overlapping poi
            }
        }
        if(isSecondary)
        {
            this.poi2Ids.push(this.allPoi.length);
        }
        this.allPoi.push(new Poi(this.allPoi.length,mesh));
        this.poiMeshes.push(mesh);
        if(!this.showPofI)
            {this.scene.remove(mesh);}
    }

    ccw(A,B,C)
    {
        return (C[1]-A[1]) * (B[0]-A[0]) > (B[1]-A[1]) * (C[0]-A[0])
    }

    segCollision(A,B,C,D)
    {
        return this.ccw(A,C,D) != this.ccw(B,C,D) && this.ccw(A,B,C) != this.ccw(A,B,D)
    }

    update()
    {
        this.triangulation = Delaunator.from(this.allPoi,(poi)=>poi.x,(poi)=>poi.y);

        //get all edges
        let paths = [];
        for(let i = 0; i < this.triangulation.triangles.length; i+=3)
        {
            paths.push([this.triangulation.triangles[i], this.triangulation.triangles[i+1]])
            paths.push([this.triangulation.triangles[i+1], this.triangulation.triangles[i+2]])
            paths.push([this.triangulation.triangles[i+2], this.triangulation.triangles[i]])
        }
        
        //remove duplicates
        let uniquePaths = []
        paths.forEach(tuple =>
        {
            let unique = true;

            uniquePaths.some(function(t,indx)
            {
                if((t[0] == tuple [0] && t[1] == tuple[1]) || (t[0] == tuple [1] && t[1] == tuple[0]))
                {
                    unique = false;
                    return true;
                }
            })
            if(unique)
            {
                uniquePaths.push(tuple)
            }
        })

        //reset all
        this.triBuffGeoms.forEach( e => e.dispose());
        this.triMeshesValid.forEach( e => this.scene.remove(e));
        this.triMeshesInvalid.forEach( e => this.scene.remove(e));
        this.triBuffGeoms = [];
        this.triMeshesValid = [];
        this.triMeshesInvalid = [];
        this.validPaths = [];
        this.invalidPaths = [];

        //find valid and invalid paths
        uniquePaths.forEach( path =>
        {
            let noCollisions = true;
            let a = [this.allPoi[path[0]].x, this.allPoi[path[0]].y];
            let b = [this.allPoi[path[1]].x, this.allPoi[path[1]].y];
            for (let i = 0; i < this.hlines.length; i+=4)
            {
                let c = [this.hlines[i], this.hlines[i+1]];
                let d = [this.hlines[i+2], this.hlines[i+3]];
                if(this.segCollision(a,b,c,d))
                {
                    noCollisions = false;
                    this.invalidPaths.push(path);
                    break;
                }
            }
            if(noCollisions)
            {
                this.validPaths.push(path);
            }
        })
        

        this.invalidPaths.forEach(path =>
        {
            const a = new THREE.Vector3(this.allPoi[path[0]].x, this.allPoi[path[0]].y, -this.far+1);
            const b = new THREE.Vector3(this.allPoi[path[1]].x, this.allPoi[path[1]].y, -this.far+1);

            const g = new THREE.BufferGeometry().setFromPoints([a,b]);
            const line = new THREE.LineSegments(g, this.lineBadMaterial);
            line.computeLineDistances();
            this.triBuffGeoms.push(g);
            this.triMeshesInvalid.push(line);
            if(this.showGraph && !this.hideInvalid)
                {this.scene.add(line);}
        })
        //reset poi info
        this.allPoi.forEach(p => p.reset());
        this.validPaths.forEach(path =>
        {
            //update poi neighbors
            this.allPoi[path[0]].neighbors.push(path[1])           
            this.allPoi[path[1]].neighbors.push(path[0])
            const a = new THREE.Vector3(this.allPoi[path[0]].x, this.allPoi[path[0]].y, -this.far+1);
            const b = new THREE.Vector3(this.allPoi[path[1]].x, this.allPoi[path[1]].y, -this.far+1);
    
            const g = new THREE.BufferGeometry().setFromPoints([a,b]);
            const line = new THREE.LineSegments(g, this.lineGoodMaterial);
            this.triBuffGeoms.push(g);
            this.triMeshesValid.push(line);
            if(this.showGraph)
                {this.scene.add(line);}
        })
    }  

    //show-hide
    hide()
    {
        this.triMeshesValid.forEach( e => this.scene.remove(e));
        this.triMeshesInvalid.forEach( e => this.scene.remove(e));
        this.showGraph = false; 
    }
    show()
    {
        this.triMeshesValid.forEach( e => this.scene.add(e));    
        if(!this.hideInvalid)
            {this.triMeshesInvalid.forEach( e => this.scene.add(e));}
        this.showGraph = true;
    }
    hideInv()
    {
        this.triMeshesInvalid.forEach( e => this.scene.remove(e));
        this.hideInvalid = true;
    }
    showInv()
    {
        if(this.showGraph)
            {this.triMeshesInvalid.forEach( e => this.scene.add(e));}
        this.hideInvalid = false;
    }
    hidePoi()
    {
        this.poiMeshes.forEach( e => this.scene.remove(e));
        this.showPofI = false;                  
    }
    showPoi()
    {
        this.poiMeshes.forEach( e => this.scene.add(e));
        this.showPofI = true;
    }
}

class Visitor
{
    constructor(graph, buffGeom, mat)
    {
        //GENERATION
        this.start = 0;
        this.end = 1;
        this.current = this.start;
        
        //copy poi info
        this.allPoi = graph.allPoi;
        this.poi2Ids = graph.poi2Ids;

        //copy valid paths
        this.validPaths = graph.validPaths;
        this.allPoi[this.current].visited = true;
        
        //record traj
        this.traj = [this.start];
        this.realTraj = [[this.allPoi[this.start].x,this.allPoi[this.start].y]];
        this.visited = [this.start];
        this.done = false;
        
        
        // //ANIMATION
        this.speed = 1+Math.random()*2;
        this.next = 0;
        this.mesh = new THREE.Mesh(buffGeom, mat);     

        this.timeStamp = -1;
        this.waitTime = -1;
        this.waiting = false;
    }
    
    sqDistTo(poiId)
    {
        let l = this.realTraj.length
        return (this.realTraj[l-1][0]-this.allPoi[poiId].x)**2 + (this.realTraj[l-1][1]-this.allPoi[poiId].y)**2
    }

    weightedPick(pool)
    {
        let dists = []
        pool.forEach(poiId =>
        {
            dists.push(this.sqDistTo(poiId));
        })

        //lowest distance has higher chance
        let recDists = []
        dists.forEach(d =>
        {
            if(d>0)
                recDists.push(1.0/d);
            else
                recDists.push(0.0);
        })                

        //calc cumulative weights
        let total = recDists.reduce((a, b) => a + b, 0)
        
        let weights = []
        recDists.forEach(d =>
        {
            weights.push(d/total*1.0)
        })

        let cWeights = []
        weights.reduce(function(a,b,i) { return cWeights[i] = a+b;},0);
        
        const thresh = Math.random();
            
        for(let k = 0; k < pool.length;k++)
        {
            if(cWeights[k] >= thresh)
            {
                return pool[k]
            }
        }
    }

    getNeighborsOf(target)
    {
        return this.allPoi[target].neighbors;
    }

    getChoicesOf(target)
    {
        let choices = [];
        this.allPoi[target].neighbors.forEach( n =>
        {
            if(!this.visited.includes(n))
            {
                choices.push(n);
            }
        })
        //never choose the end if another immediate option is available
        if(choices.length > 1)
        {
            const ind = choices.indexOf(this.end);
            if(ind != -1)
            {
                choices.splice(ind,1);
            }
        }
        return choices;
    }

    gaussianRand(sigma)
    {
        let rand = 0;
        for (var i = 0; i < 6; i += 1)
        {
            rand += Math.random();
        }
        rand = rand/6;
        return rand*(sigma*2)-sigma;
    }

    normNoise(target)
    {
        let sigma = 25;
        let gx = this.gaussianRand(sigma);
        let gy = this.gaussianRand(sigma);
        
        return [this.allPoi[target].x+gx,this.allPoi[target].y+gy];
    }

    moveOn(choices)
    {
        //always prioritise primary poi
        let primary = [];
        let secondary = [];
        choices.forEach( pid =>
        {
            if(this.poi2Ids.includes(pid))
            {
                secondary.push(pid);
            }
            else
            {
                primary.push(pid);
            }
        })
        if(primary.length > 0)
        {
            this.current = this.weightedPick(primary);
        }
        else
        {
            this.current = this.weightedPick(secondary);
        }
        this.visited.push(this.current);
        this.traj.push(this.current);
        this.realTraj.push(this.normNoise(this.current));
    }

    bfs()
    {
        let q = [];
        q.push(this.current);
        let origin = this.current;
        let checked = [this.current];
        while(q.length > 0)
        {
            let val = q.shift();
            for(let i = 0; i < this.allPoi[val].neighbors.length; ++i)
            {
                let nbr = this.allPoi[val].neighbors[i];
                if(!checked.includes(nbr))
                {
                    checked.push(nbr);
                    this.allPoi[nbr].parent = this.allPoi[val].id//record how i got there
                    if(!this.visited.includes(nbr))
                    {
                        let goto = nbr;
                        let fromHere = this.allPoi[nbr].parent;
                        while(fromHere != origin)
                        {
                            goto = fromHere;
                            fromHere = this.allPoi[fromHere].parent
                        }
                        //the next node we want to move to, first in the path towards the unvisited neighbour we just found
                        return goto;
                    }
                    else
                    {
                        q.push(nbr);
                    }
                }
            }
        }
        //Should never get here with a correct graph.
        console.log("Start and End are not connected")
        this.done = true;       
        return this.current;
    }

    step()
    {
        let choices = this.getChoicesOf(this.current);
        if(choices.length > 0)//if there are unvisited neighbors
        {
            this.moveOn(choices);
        }
        else//use BFS to find path to unvisited neighbor
        {
            this.current = this.bfs();
            this.traj.push(this.current);
            this.realTraj.push(this.normNoise(this.current))
        }
        if(this.current == this.end)
        {
            this.done = true;
        }
    }

    initialise(scene,z)
    {
        //generate trajectory
        while(!this.done)
        {
            this.step()                
        }
        //create mesh    
        this.mesh.position.x = this.realTraj[0][0];
        this.mesh.position.y = this.realTraj[0][1];
        this.mesh.position.z = z;
        scene.add(this.mesh);
    }

    testGraph()//check start and end are connected
    {
        let q = [];
        q.push(this.current);
        let origin = this.current;
        let checked = [this.current];
        while(q.length > 0)
        {
            let val = q.shift();
            for(let i = 0; i < this.allPoi[val].neighbors.length; ++i)
            {
                let nbr = this.allPoi[val].neighbors[i];
                if(!checked.includes(nbr))
                {
                    checked.push(nbr);
                    this.allPoi[nbr].parent = this.allPoi[val].id//record how i got there
                    if(nbr == this.end)
                    {
                        return true;//start and end are connected
                    }
                    else
                    {
                        q.push(nbr);
                    }
                }
            }
        }
        this.done = true;       
        return false;//start and end are not connected
    }

    animate(time,dt)
    {           
        const xdiff = this.mesh.position.x - this.realTraj[this.next][0]
        const ydiff = this.mesh.position.y - this.realTraj[this.next][1]
        const hyp = Math.sqrt(xdiff**2 + ydiff**2)
        if(hyp > this.speed*(dt/16) && !this.waiting)
        {
            this.mesh.position.x -= this.speed * (xdiff/hyp) * (dt/16)//1 frame = 16ms at 60 fps
            this.mesh.position.y -= this.speed * (ydiff/hyp) * (dt/16)
        }
        else
        {
            if(this.waiting)
            {
                const elapsed = time - this.timeStamp;

                if(elapsed > this.waitTime)
                {
                    this.waiting = false;
                }
            }
            else
            {         
                if(this.poi2Ids.includes(this.traj[this.next]))//if we're at a secondary node
                {
                    this.waitTime = 0;
                    this.waiting = false;
                }
                else
                {
                    this.timeStamp = time;
                    this.waitTime = Math.random();
                    this.waiting = true;
                }
                
                if(this.next < this.realTraj.length-1)
                {   
                    this.next += 1                 
                }
                else//done walking
                {
                    return true;
                }   
            }
        }
        return false;//not done walking
    }
}

export {Poi, DelaunayGraph, Visitor};
