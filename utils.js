import * as THREE from './lib/three.module.js';

function makeInstance(thescene,geometry,material,pos,rot)
{               
    const mesh = new THREE.Mesh(geometry, material);
    thescene.add(mesh);
    mesh.position.x = pos[0];
    mesh.position.y = pos[1];
    mesh.position.z = pos[2];
    mesh.rotation.x = rot[0];
    mesh.rotation.y = rot[1];
    mesh.rotation.z = rot[2];
    return mesh;
}

function resizeRendererToDisplaySize(renderer)
{
    const canvas = renderer.domElement;
    const width = canvas.clientWidth;
    const height = canvas.clientHeight;
    const needResize = canvas.width !== width || canvas.height !== height;
    if (needResize) {height
    renderer.setSize(width, height, false);
    }
    return needResize;
}

//Get Hough lines with OpenCV
function getHoughLines(floorPlanId)
{
    let src = cv.imread(floorPlanId);
    let blur = new cv.Mat();
    let edges = new cv.Mat();
    let lines = new cv.Mat();
    // let dst = cv.Mat.zeros(src.rows, src.cols, cv.CV_8UC3);

    cv.cvtColor(src, blur, cv.COLOR_RGB2GRAY, 0);

    let ksize = new cv.Size(3, 3);
    cv.GaussianBlur(blur, blur, ksize, 0, 0, cv.BORDER_DEFAULT);

    cv.Canny(blur, edges, 100,200,3,false)
    cv.HoughLinesP(edges, lines, 1, Math.PI / 180, 15, 5, 5);

    // //Render the lines on an empty matrix
    // let color = new cv.Scalar(255, 0, 0);
    // for (let i = 0; i < lines.rows; ++i)
    // {
    //     let startPoint = new cv.Point(lines.data32S[i * 4], lines.data32S[i * 4 + 1]);
    //     let endPoint = new cv.Point(lines.data32S[i * 4 + 2], lines.data32S[i * 4 + 3]);
    //     cv.line(dst, startPoint, endPoint, color);
    // }
    // cv.imshow("c",dst);
    
    const rval = lines.data32S;

    src.delete();
    blur.delete();
    edges.delete();
    lines.delete();
    // dst.delete();

    return rval;         
};

export {getHoughLines, makeInstance, resizeRendererToDisplaySize};