import * as THREE from './lib/three.module.js';
import {DelaunayGraph, Visitor} from './classes.js';
import {getHoughLines, makeInstance, resizeRendererToDisplaySize} from './utils.js';

window.main = function(floorplanId)
{
    //scene setup
    const canvas = document.querySelector('#c');
    const renderer = new THREE.WebGLRenderer({canvas});
    const fpwidth = document.getElementById(floorplanId).width;//floorplan width px
    const fpheight = document.getElementById(floorplanId).height;//floorplan height px
    const near = 0;
    const far = 500;
    const camera = new THREE.OrthographicCamera(0, fpwidth, fpheight, 0, near, far);
    const scene = new THREE.Scene();
    const light = new THREE.DirectionalLight(0xFFFFFF, 1);
    light.position.set(0, 0, 500);
    scene.add(light);    

    //stats widget
    let stats = new Stats();
    stats.showPanel(0);
    let showStats = false;
    stats.domElement.style.display = 'none';
    document.body.appendChild( stats.domElement );
    document.addEventListener('keydown', function(e)
    {
        if(e.key == 'p')
        {
            e.preventDefault();
            showStats = !showStats;
            if(showStats)
                {stats.domElement.style.display = '';}
            else
                {stats.domElement.style.display = 'none';}
        }
    });

    //define materials
    const loader = new THREE.TextureLoader();
    const floorPlanMaterial = new THREE.MeshStandardMaterial({map: loader.load(document.getElementById(floorplanId).src)});
    const primaryPoiMaterial = new THREE.MeshPhongMaterial({color:0xFF0000});
    const secondaryPoiMaterial = new THREE.MeshPhongMaterial({color:0x00FF00});  
    const startMaterial = new THREE.MeshPhongMaterial({map: loader.load('./images/start.png')});
    const endMaterial = new THREE.MeshPhongMaterial({map: loader.load('./images/end.png')});
    const visitorMaterial = new THREE.MeshPhongMaterial({color:0x00BBFF});
    const lineEdgeMaterial = new THREE.LineBasicMaterial({color: 0xff00ff,linewidth: 2});
    //define geometry
    const floorPlanGeom = new THREE.PlaneBufferGeometry(fpwidth,fpheight,1,1);
    const radius = 5;
    const primaryPoiGeom = new THREE.ConeBufferGeometry(radius,radius,4);
    const secondaryPoiGeom = new THREE.BoxBufferGeometry(radius*1.3,radius*1.3,radius*1.3);
    const startEndGeom = new THREE.CircleBufferGeometry(radius*1.5, 30);
    const visitorGeom = new THREE.SphereBufferGeometry(radius,30,30);
    //convenience functions
    function makePoi1(pos){return makeInstance(scene, primaryPoiGeom,primaryPoiMaterial,pos,[1.5,0,0])}
    function makePoi2(pos){return makeInstance(scene, secondaryPoiGeom,secondaryPoiMaterial,pos,[1.5,0,0])}
    function makeStart(pos){return makeInstance(scene, startEndGeom,startMaterial,pos,[0,0,0])}
    function makeEnd(pos){return makeInstance(scene, startEndGeom,endMaterial,pos,[0,0,0])}

    //edge detection
    const hlines = getHoughLines(floorplanId);
    let hlinesBuffGeoms = [];
    let hlinesMeshes = [];

    //console.log(hlines.length);
    
    for (let i = 0; i < hlines.length; i+=4)
    {
        //for some reason the y coordinate is flipped...
        hlines[i+1] = fpheight-hlines[i+1];
        hlines[i+3] = fpheight-hlines[i+3];
        
        //create geom+mesh for detected edges
        let a = new THREE.Vector3(hlines[i], hlines[i+1],-far+1);
        let b = new THREE.Vector3(hlines[i+2], hlines[i+3], -far+1);
        hlinesBuffGeoms.push(new THREE.BufferGeometry().setFromPoints([a,b]));
        hlinesMeshes.push(new THREE.LineSegments(hlinesBuffGeoms[hlinesBuffGeoms.length-1], lineEdgeMaterial));
    }

    // /make secondary render target to render the detected edges
    const renderTarget = new THREE.WebGLRenderTarget(fpwidth, fpheight);
    const rtScene = new THREE.Scene();
    //add the lines to the rtscene and render them on the rt
    hlinesMeshes.forEach( e => rtScene.add(e));
    renderer.setRenderTarget(renderTarget);
    renderer.render(rtScene, camera);
    renderer.setRenderTarget(null);
    //draw the rt to a plane for use in main scene
    const hlineTextMat = new THREE.MeshStandardMaterial({map: renderTarget.texture, transparent: true});
    let fpHlines = makeInstance(scene,floorPlanGeom,hlineTextMat,[fpwidth/2,fpheight/2,-far],[0,0,0]);
    if (!document.getElementById("showDetectedWalls").checked)
        {scene.remove(fpHlines);}
    //dispose geom and rm mesh
    hlinesBuffGeoms.forEach(e => e.dispose());
    hlinesMeshes.forEach(e => rtScene.remove(e));   

    //draw floor plan
    makeInstance(scene,floorPlanGeom,floorPlanMaterial,[fpwidth/2,fpheight/2,-far],[0,0,0]);

    //Setup delaunay graph and visitors
    let dGraph = new DelaunayGraph(hlines,far,scene);
    
    //CHECKBOXES
    window.showDetectedWallsF = function()
    {
        if (document.getElementById("showDetectedWalls").checked)
        {scene.add(fpHlines);}
        else
        {scene.remove(fpHlines);}
    }
    window.showGraphF = function()
    {
        if (document.getElementById("showGraph").checked)
        {dGraph.show();}
        else
        {dGraph.hide();}
    }
    window.hideInvalidEdgesF = function()
    {
        if (document.getElementById("hideInvalidEdges").checked)
        {dGraph.hideInv();}
        else
        {dGraph.showInv();}
    }
    window.showPoiF = function()
    {
        if (document.getElementById("showPoi").checked)
        {dGraph.showPoi();}
        else
        {dGraph.hidePoi();}
    }

    //BUTTONS
    //select floor plan
    let fpselect = document.getElementById("dropdown1");
    fpselect.onchange = function(e)
    {
        e.preventDefault();
        switch(fpselect.value)
        {
            case "0":
                window.sessionStorage.setItem("fpID","fp0");
                location.reload();
                break;
            case "1":
                window.sessionStorage.setItem("fpID","fp1");
                location.reload();
                break;
            case "2":
                window.sessionStorage.setItem("fpID","fp2");
                location.reload();
                break;
            case "3":
                window.sessionStorage.setItem("fpID","fp3");
                location.reload();
                break;
            default:
              break;// code block
        }
    };
    //start/end button
    let seToolOn = true;
    let buttonSE = document.createElement("button");
    buttonSE.innerHTML = "Set Start/End";
    document.getElementById("btnstartend").appendChild(buttonSE);
    buttonSE.addEventListener("click", function(e)
    {
        e.preventDefault();
        seToolOn = true;
        document.getElementById('info').innerHTML = window.instructions+window.seToolInfo;
    });
    //Add poi button
    let buttonPOI = document.createElement("button");
    buttonPOI.innerHTML = "Add POI";
    document.getElementById("btnpoi").appendChild(buttonPOI);
    buttonPOI.addEventListener("click", function(e)
    {
        e.preventDefault();
        seToolOn = false;
        document.getElementById('info').innerHTML = window.instructions+window.poiToolInfo;
    });
    //Start button
    let animate = false;
    let buttonStart = document.createElement("button");
    buttonStart.innerHTML = "Start";
    document.getElementById("btnstart").appendChild(buttonStart);
    buttonStart.addEventListener("click", function(e)
    {
        e.preventDefault();
        if(dGraph.allPoi.length > 2)//if there's a minimum graph
        {
            let tester = new Visitor(dGraph, visitorGeom, visitorMaterial);
            if(tester.testGraph())
            {
                animate = !animate;
                if(animate)
                    {buttonStart.innerHTML = "Pause";}
                else
                    {buttonStart.innerHTML = "Start";}
                if(seToolOn)
                    {document.getElementById('info').innerHTML = window.instructions+window.seToolInfo}
                else
                    {document.getElementById('info').innerHTML = window.instructions+window.poiToolInfo}
            }
            else
            {
                if(seToolOn)
                    {document.getElementById('info').innerHTML = window.instructions+window.seToolInfo}
                else
                    {document.getElementById('info').innerHTML = window.instructions+window.poiToolInfo}
                document.getElementById('info').innerHTML += window.seDiscoWarning;
            }
        }
        else
        {
            if(seToolOn)
                {document.getElementById('info').innerHTML = window.instructions+window.seToolInfo}
            else
                {document.getElementById('info').innerHTML = window.instructions+window.poiToolInfo}
            document.getElementById('info').innerHTML += window.makeGWarning;

        }
    });
    //Reset anim button
    let visitors = [];
    let buttonReset = document.createElement("button");
    buttonReset.innerHTML = "Reset animation";
    document.getElementById("btnreset").appendChild(buttonReset);
    buttonReset.addEventListener("click", function(e)
    {
        e.preventDefault();
        visitors.forEach(v =>
        {
            scene.remove(v.mesh);
        });
        visitors = [];
        animate = false;
        buttonStart.innerHTML = "Start";
    });
    //Reset graph button
    let buttonResetG = document.createElement("button");
    buttonResetG.innerHTML = "Reset graph";
    document.getElementById("btnresetG").appendChild(buttonResetG);
    buttonResetG.addEventListener("click", function(e)
    {
        e.preventDefault();

        if(animate || visitors.length > 0)
        {
            if(seToolOn)
                {document.getElementById('info').innerHTML = window.instructions+window.seToolInfo+window.resFirstWarning}
            else
                {document.getElementById('info').innerHTML = window.instructions+window.poiToolInfo+window.resFirstWarning}
        }
        else
        {
            //cleanup all
            dGraph.triBuffGeoms.forEach( e => e.dispose());
            dGraph.triMeshesValid.forEach( e => scene.remove(e));
            dGraph.triMeshesInvalid.forEach( e => scene.remove(e));
            dGraph.allPoi.forEach(e => scene.remove(e.mesh));
            //remember checked values
            const sg = dGraph.showGraph;
            const hi = dGraph.hideInvalid;
            const sp = dGraph.showPofI;
            dGraph = new DelaunayGraph(hlines,far,scene);
            dGraph.showGraph = sg;
            dGraph.hideInvalid = hi;
            dGraph.showPofI = sp;
        }
    });

    //handle mouse input on canvas
    c.addEventListener('mousedown', onMouseDown, false);
    function onMouseDown(e)
    {
        e.preventDefault();

        if(animate || visitors.length > 0)
        {
            if(seToolOn)
                {document.getElementById('info').innerHTML = window.instructions+window.seToolInfo+window.resFirstWarning}
            else
                {document.getElementById('info').innerHTML = window.instructions+window.poiToolInfo+window.resFirstWarning}
        }
        else
        {
            //calculate click coordinates
            let canvasBounds = canvas.getBoundingClientRect();
            let x = ( ( e.clientX - canvasBounds.left ) / ( canvasBounds.right - canvasBounds.left ) ) * 2 - 1;
            let y = - ( ( e.clientY - canvasBounds.top ) / ( canvasBounds.bottom - canvasBounds.top) ) * 2 + 1;
            let vector = new THREE.Vector3();
            vector.set(x,y,0).unproject(camera);
            
            if(seToolOn)//set start/end
            {
                if(e.button == 0)
                {
                    dGraph.setStart(makeStart([vector.x,vector.y,-far+radius]));
                    document.getElementById('info').innerHTML = window.instructions+window.seToolInfo;
                }
                else if(e.button == 2)
                {
                    if(dGraph.allPoi.length > 0)
                    {
                        dGraph.setEnd(makeEnd([vector.x,vector.y,-far+radius]));
                    }
                    else
                    {
                        document.getElementById('info').innerHTML = window.instructions+window.seToolInfo+window.sFirstWarning;
                    }
                }        
                dGraph.update();   
            }
            else//add POI
            {
                if(dGraph.allPoi.length > 1)// start and end are set;
                {
                    //create POI if inside canvas
                    if(e.button == 0)
                    {
                        dGraph.addPoi(makePoi1([vector.x,vector.y,-far+radius]), false);
                    }
                    else if(e.button == 2)
                    {
                        dGraph.addPoi(makePoi2([vector.x,vector.y,-far+radius]), true);
                    }           
                    dGraph.update();
                }
                else
                {
                    document.getElementById('info').innerHTML = window.instructions+window.poiToolInfo+window.noSEWarning;
                }
            }
        }
    }

    //visitor animation
    let dt = 16;
    let timeStamp = 0;
    let maxSpawnInterval = 2;
    let spawnInterval = Math.random()*maxSpawnInterval;
    function animationLogic(time)
    {
        if(time > timeStamp + spawnInterval)
        {
            timeStamp = time;
            spawnInterval = Math.random()*maxSpawnInterval;

            visitors.push(new Visitor(dGraph, visitorGeom, visitorMaterial))
            visitors[visitors.length-1].initialise(scene,-far+radius);
        }
        let toRemove = []
        visitors.forEach((v) =>
        {
            //traj over, remove
            if(v.animate(time,dt))
            {
                toRemove.push(v)
            }
        });
        toRemove.forEach(v =>
        {
            scene.remove(v.mesh)
            visitors.splice(visitors.indexOf(v), 1);
        })
    }

    let lastCalledTime = performance.now();
    function mainLoop(time) 
    {
        stats.begin();
        time *= 0.001;

        if (resizeRendererToDisplaySize(renderer))
        {
            const canvas = renderer.domElement;
            camera.aspect = canvas.clientWidth / canvas.clientHeight;
            camera.updateProjectionMatrix();
        }

        if(animate)
        {
            animationLogic(time);
        }
        renderer.render(scene, camera);

        dt = (performance.now() - lastCalledTime);
        lastCalledTime = performance.now();

        stats.end();
        
        requestAnimationFrame(mainLoop);
    }

    requestAnimationFrame(mainLoop);
}



