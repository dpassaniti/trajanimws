document.getElementById('info').innerHTML = "<span style='color:#00ff00'>LOADING...</span>";
requirejs(["./lib/opencv.js"], function()
{
    window.instructions = "<span style='color:#00ff00'>INSTRUCTIONS</span><br/>\
    1: <span style='color:#00ffff'>Select a floor plan</span> from the dropdown list.<br/>\
    2: Use the <span style='color:#00ffff'>Set Start/End</span> tool to set START and END locations.<br/>\
    3: Use the <span style='color:#00ffff'>Add POI</span> tool to set points of interest.<br/>\
    4: Click <span style='color:#00ffff'>Start</span> to begin the animation.<br/><br/>"

    window.seToolInfo = "<span style='color:#00ff00'>CURRENT TOOL: Set Start/End</span><br/>\
    > <span style='color:#00ffff'>Left click</span> to set the start location.<br/>\
    > <span style='color:#00ffff'>Right click</span> to set the end location.<br/>";

    window.poiToolInfo = "<span style='color:#00ff00'>CURRENT TOOL: Add POI</span><br/>\
    > <span style='color:#00ffff'>Left click</span> to add a primary POI.\
    Used for important poi that visitors will prioritise and pause at (e.g. a museum item). <br/>\
    > <span style='color:#00ffff'>Right click</span> to add a secondary POI.\
    Used for less important poi (e.g. a doorway), or to just reshape the graph.<br/>";

    window.noSEWarning = "<br/><span style='color:#ff0000'>YOU MUST SET START AND END BEFORE ADDING A POI !!!</span><br/>\
    Select the <span style='color:#00ffff'>Set Start/End</span> tool."

    window.seDiscoWarning = "<br/><span style='color:#ff0000'>START AND END ARE NOT CONNECTED !!!</span><br/>\
    Change their location or add POI to connect them before starting the animation.";

    window.sFirstWarning = "<br/><span style='color:#ff0000'>SET START BEFORE SETTING END !!!</span><br/>\
    <span style='color:#00ffff'>Left click</span> on the floor plan to add the start location.";

    window.resFirstWarning = "<br/><span style='color:#ff0000'>RESET THE ANIMATION BEFORE MODIFYING THE GRAPH !!!</span><br/>\
    Click the <span style='color:#00ffff'>Reset animation</span> button";

    window.makeGWarning = "<br/><span style='color:#ff0000'>CREATE A GRAPH BEFORE STARTING THE ANIMATION !!!</span><br/>\
    The minimum graph is start, end and 1 POI."

    let fpid = "fp0";
    if (sessionStorage.getItem("fpID") != null)
    {
        fpid = sessionStorage.getItem("fpID");
    }
    cv['onRuntimeInitialized']=()=>
    {
        document.getElementById('info').innerHTML = window.instructions+window.seToolInfo;
        main(fpid);//defined in editor.js. load that script as module before require.js       
    }
});